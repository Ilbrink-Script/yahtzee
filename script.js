const diceQty = 5;
const bonusLimit = 63;
const bonusPoints = 35;
const maxRolls = 6 + 7;
let rollCount = 0;


// rolls a number of dices and returns the sorted result
const rollDices = (num = diceQty) => {
    const rollArr = [];
    for (let i = 0; i < num; i++) {
        rollArr.push(Math.ceil(Math.random() * 6));
    }
    return rollArr.sort();
};

// converts
const getAggregateDices = (dices) => {
    const aggrDices = {};
    for (let i = 1; i <= 6; i++) {
        aggrDices[i] = 0;
    }
    dices.forEach(dice => {
        aggrDices[dice]++;
    });
    return aggrDices;
};

// takes an array of dice values and renders it in the table
const renderRoll = dices => {
    const rollCells = document.querySelectorAll('#roll td');
    for (let i = 0; i < dices.length; i++) {
        rollCells[i].innerText = dices[i];
    }
};

// updates score in scoresheet, but only if the score cell isn't locket yet
const setScore = (element, points) => {
    if (!element.classList.contains('lock')) {
        element.innerText = points > 0 ? points : '-';
    }
};

// loop the dice entries and update scores in part 1
const handleNumScores = aggrDices => {
    const numScoreCells = document.getElementsByClassName('num');
    const diceEntries = Object.entries(aggrDices);

    for (const [num, count] of diceEntries) {
        const points = parseInt(num) * count;
        const scoreElement = numScoreCells[num - 1];
        setScore(scoreElement, points);
    }
};

// handles aggregate score, sum of all dices if there are a certain amount of duplicates
const handleAggregateScore = (elementId, minDuplicates, aggrDices, fixedPoints) => {
    const scoreElement = document.getElementById(elementId);

    let maxDuplicates = 0;
    let sum = 0;
    for (const [num, duplicates] of Object.entries(aggrDices)) {
        sum += num * duplicates;
        maxDuplicates = Math.max(maxDuplicates, duplicates);
    }

    const enoughDuplicates = maxDuplicates >= minDuplicates;
    const tmpPoints = fixedPoints > 0 ? fixedPoints : sum;
    const points = enoughDuplicates ? tmpPoints : 0;

    setScore(scoreElement, points);

    // YAHTZEE !!!!
    if (maxDuplicates == diceQty) {
        showMessage('Yippee Yahtzee motherf*cker', party);
    }
};

// hangle full house score, 25 points for 3 + 2
const handleFullHouseScore = aggrDices => {
    const diceDuplicates = Object.values(aggrDices);
    const double = diceDuplicates.includes(2);
    const triple = diceDuplicates.includes(3);

    const points = double && triple ? 25 : '-';
    const scoreElement = document.getElementById('full-house');

    setScore(scoreElement, points);
};

// handle street score, requires an specified amount of consecutive dice values
const handleStreetScore = (elementId, consecutiveNum, fixedPoints, aggrDices) => {
    let consecutive = 0;
    let maxConsecutive = 0;
    const diceOccurances = Object.values(aggrDices);

    // loop through the dice occurances
    diceOccurances.forEach(diceCount => {
        if (diceCount > 0) {
            consecutive++;
            maxConsecutive = Math.max(consecutive, maxConsecutive);
        } else {
            consecutive = 0;
        }
    });

    // check if enough consecutive dices were found
    const points = maxConsecutive >= consecutiveNum ? fixedPoints : '-';
    const scoreElement = document.getElementById(elementId);

    setScore(scoreElement, points);
};

// handle the various combo scores for the bottom of the score sheet
const handleComboScores = aggrDices => {
    handleAggregateScore('three-of-a-kind', 3, aggrDices);
    handleAggregateScore('carre', 4, aggrDices);

    handleFullHouseScore(aggrDices);

    handleStreetScore('small-street', 4, 30, aggrDices);
    handleStreetScore('large-street', 5, 40, aggrDices);

    handleAggregateScore('yahtzee', 5, aggrDices, 50);
    handleAggregateScore('chance', 0, aggrDices);
};

// returns an object with all possible scores for this roll
const handleScores = dices => {
    const aggrDices = getAggregateDices(dices);
    handleNumScores(aggrDices);
    handleComboScores(aggrDices);
};

// calculates the score for scores with the provided scoreClass
const calculateTotalByClass = scoreClass => {
    let total = 0;

    // add the locked scores
    const lockedScores = document.getElementsByClassName(`${scoreClass} score lock`);
    for (const scoreCell of lockedScores) {
        const score = Number.parseInt(scoreCell.innerText);
        if (Number.isInteger(score))  {
            total += score;
        }
    }

    return total;
};

// calculates the numeric total for part 1 of the score sheet
const calculateNumTotal = () => {
    const subtotal = calculateTotalByClass('num');
    let total = subtotal;

    // calculate bonus
    let bonusValue = '-';
    if (subtotal >= bonusLimit) {
        bonusValue = bonusPoints;
        total += bonusPoints;
    }

    // update the subtotal elements
    document.getElementById('subtotal-1').innerText = subtotal;
    document.getElementById('bonus').innerText = bonusValue;
    for (const element of document.getElementsByClassName('total-1')) {
        element.innerText = total;
    }

    return subtotal;
};

const calculateComboTotal = () => {
    const total = calculateTotalByClass('combo');

    // update subtotal element
    document.getElementById('total-2').innerText = total;

    return total;
};

// calculate the various totals
const calculateTotals = () => {
    const numTotal = calculateNumTotal();
    const comboTotal = calculateComboTotal();
    const total = numTotal + comboTotal;

    // update element
    document.getElementById('total').innerText = total;
};


// handles the roll
const handleRoll = dices => {
    renderRoll(dices);
    handleScores(dices);
    calculateTotals();
};

// toggles the class for element but only allows 1 instance in the DOM
const toggleUniqueClass = (element, className) => {
    const hasClass = element.classList.contains(className);

    // remove all elements that have this class
    for (const el of document.getElementsByClassName(className)) {
            el.classList.remove(className);
    }

    if (!hasClass) {
        element.classList.add(className);
    }
};

// looks for elements with oldClass and replaces it with newClass
const replaceClass = (oldClass, newClass) => {
    for (const el of document.getElementsByClassName(oldClass)) {
        el.classList.remove(oldClass);
        el.classList.add(newClass);
    }
};

// listens to roll button clicks
const addRollBtnListener = () => {
    const btn = document.getElementById('rollBtn');
    btn.addEventListener('click', event => {
        const isTmpLock = document.getElementsByClassName('tmpLock').length > 0;

        // check if there has been a roll already
        if (rollCount > 0) {
            if (!isTmpLock) {
                showMessage('Zet eerst een score vast door erop te klikken.\nDaarna kan je weer gooien.', 'warning');
                return false;
            } else {
                showMessage('Zet een score vast door erop te klikken.\nDaarna kan je weer gooien.', 'info');
            }
        }

        // keep track of the amount of rolls
        rollCount++;

        // fixates the locks
        replaceClass('tmpLock', 'lock');

        const dices = rollDices();
        console.log(`${rollCount}: Dices were rolled`, dices);

        // final roll, we can manually pick the score to use
        if (rollCount == maxRolls) {
            const finalScore = document.querySelector('.score:not(.lock)');
            finalScore.classList.add('lock');

            showMessage('Het spel is over.\nKlik op Reset om een nieuw spel te starten.');
            toggleButtonStatus();
        }

        handleRoll(dices);
    });
};

// handles reset button
const addResetBtnListener = () => {
    const btn = document.getElementById('resetBtn');
    btn.addEventListener('click', event => {
        resetGame();
    });
}

// toggles disabled property on buttons
const toggleButtonStatus = () => {
    for (const btn of document.getElementsByTagName('button')) {
        if (btn.disabled) {
            btn.removeAttribute('disabled');
        } else {
            btn.setAttribute('disabled', '');
        }
    };
};


// score listener handles clicks on score columns and (un)locks it temporarely
const addScoreListener = () => {
    const scoreElements = document.getElementsByClassName('score');

    for (const element of scoreElements) {
        element.addEventListener('click', event => {
            // the game must have started and it can't be locked already
            if (!element.classList.contains('lock')) {
                if (rollCount > 0) {
                    toggleUniqueClass(element, 'tmpLock');
                } else {
                    showMessage('Je moet eerst Gooien! voor je een score kan vastzetten.', 'info');
                }

            }
        });
    };
};

// display message in the message box, it can be optionally styled by classes like `warning` or `error`
const showMessage = (msg, msgClass) => {
    const msgBox = document.getElementById('message');
    msgBox.innerText = msg;
    msgBox.setAttribute('class', msgClass);
};

// (re)sets the game interface
const resetGame = () => {
    showMessage('Welkom bij Yahtzee.\n Maak na elke worp de keuze voor de te gebruiken score.', 'info');
    addRollBtnListener();
    addResetBtnListener();
    addScoreListener();
};

resetGame();